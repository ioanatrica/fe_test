
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { moduleFeatureKey } from ".";
import { CreditCard } from "../models/credit-card.interface";
import { featureKey, PaymentState } from "./reducer";

export const selectCreditCard = (state): PaymentState => state[moduleFeatureKey][featureKey];
const getPaymentState = createSelector(selectCreditCard, state => state);
const getCreditCard = createSelector(selectCreditCard, state => state.creditCardData);

export const CreditCardQuery = {
  selectCreditCard,
  getCreditCard,
  getPaymentState
};
